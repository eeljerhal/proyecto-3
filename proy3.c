#include <stdio.h>
#include <stdlib.h>
#include "carreras.h"

int cant_carr = 53;
int cant_fac = 10;
/********************************************************************/
void mostrarFacultades(facultad fac[]);
int buscarCarrera(int i, int cod, carrera carr[]);
void mostrarInfoPonderacion(int i, carrera carr[]);
void consultarPonderacion(carrera carr[]);
void mostrarPonderaciones(facultad fac[], carrera carr[]);
void mostrarInfoPuntajes(float div, int psu_lm, int puntaje, int nem, int rank, int leng, int mate, int hist, int cs, int i, carrera carr[]);
void calcularPonderacion(int nem, int rank, int leng, int mate, int hist, int cs, int i, carrera carr[]);
void simularPostulacion(carrera carr[]);
/********************************************************************/

void mostrarFacultades(facultad fac[]){	//funcion que muestra los nombres de las facultades 
	int i = 0;
	printf("\n\n");	//en donde se recorre el arreglo que los contiene y se despliega la info por pantalla
	while(i < cant_fac){
		printf("%d.\t%s\n", fac[i].cod_fac, fac[i].nom_fac);
		i++;
	}
}

int buscarCarrera(int i, int cod, carrera carr[]){	//funcion en la que se recorre el arreglo para buscar una carrera por su codigo
	while((i < cant_carr) && (carr[i].cod_carr != cod)){
		i++;
	}
	return i;
}

void mostrarInfoPonderacion(int i, carrera carr[]){	//funcion que muestra la informacion de los porcentajes de nem, rank, etc de la carrera seleccionada por pantalla
	printf("\n\n\t\t============================================\n");
	printf("\t\t\t%s\n\n\t\tNEM: %d%\tRANK: %d%\tLENG: %d%\n\t\tMATE: %d%\tHIST: %d%\tCIENCIAS:%d%\n", 
		carr[i].nom_carr, carr[i].nem, carr[i].rank, carr[i].leng, carr[i].mate, carr[i].hist, carr[i].cs);
	printf("\t\tPUNT. MIN: %.2f\t\tCUPOS: %d\n", carr[i].pond_min, carr[i].cupo_psu);
	printf("\t\t============================================\n");
}


void consultarPonderacion(carrera carr[]){	//funcion que muestra la info de las ponderaciones de una carrera
	int encontrado = 0;
	do{
		int cod, i = 0;
		printf("\n\tIntroduzca codigo de carrera: ");
		scanf("%d", &cod);
		i = buscarCarrera(i, cod, carr);	//se ingresa el codigo de la carrera y se llama a la funcion buscar carrera
		if(i != cant_carr){
			mostrarInfoPonderacion(i, carr);	//al ser encontrado se llama a la funcion mostrarinfoponderacion
			encontrado = 1;
		}else{
			printf("Codigo de carrera no encontrado, por favor reingrese\n");	//si no se encuentra es por que se ingreso un codigo de carrera erroneo y se muestra un mensaje de notificacion al usuario
		}
	}while(encontrado == 0);
}

void mostrarPonderaciones(facultad fac[], carrera carr[]){	//funcion en la cual muestra las ponderaciones de todas las carreras de una facultad
	int opt = 1;
	do{
		int i = 0;
		mostrarFacultades(fac);	//se llama a la funcion mostrar facultades con la cual se forma un menu de seleccion
		printf("11.\tSALIR\n");
		printf("\nSeleccione una facultad: ");
		scanf("%d", &opt);
		if(opt >= 1 && opt <= 10){
			printf("Ponderaciones para la: %s\n\n", fac[opt-1].nom_fac);	//se muestra por pantalla toda la info de las ponderaciones de cada carrera que contenga dicho "codigo de facultad" llamando a la funcion mostrarinfoponderacion para el despliege de info
			while((i < cant_carr)){
				if(carr[i].cod_fac == opt){
					mostrarInfoPonderacion(i, carr);
				}
				i++;
			}
		}else{
			if(opt != 11){
				printf("\n\n\t***Error, Ingrese opcion valida***\n\n");	//se notifica al usuario si la opcion seleccionada es invalida
			}
		}
	}while(opt != 11);
}

//funcion que muestra los puntajes obtenidos (ingresados previamente por el usuario), para tener una mejor visual de cada dato
void mostrarInfoPuntajes(float div, int psu_lm, int puntaje, int nem, int rank, int leng, int mate, int hist, int cs, int i, carrera carr[]){
	int punt_hist, punt_cs, no_postula_hist = 0, no_postula_cs = 0;
	printf("\t\t\t\tTUS PUNTAJES\n\n");
	printf("\t\tNEM: %d\tRANK: %d\tLENG: %d\n\t\tMATE: %d\tHIST: %d\tCIENCIAS:%d\n", 
		nem, rank, leng, mate, hist, cs);
	printf("\t\t============================================\n");
	if(carr[i].hist != 0){
		punt_hist = puntaje + hist*((float)carr[i].hist/div);	//se hace excepcion del puntaje con la psu de historia y ciencias, 
		printf("\t\tPUNT. SIMULADO PSU DE HIST.: %d\n", punt_hist);	//en donde para cada una de las opciones se muestra por pantalla al usuario si es valida su postulacion
		printf("\t\tDIFERENCIA ULTIMO SELEC.: %d\n", (int)(punt_hist - carr[i].pond_min));	//con los puntajes obtenidos en ambas pruebas por separado (en caso de que la carrera tenga la opcion de que se pueda postular con ambas)
		printf("\t\t============================================\n");
		if(punt_hist < carr[i].pond){
			no_postula_hist = 1;
		}
	}
	if(carr[i].cs != 0){
		punt_cs = puntaje + cs*((float)carr[i].cs/div);
		printf("\t\tPUNT. SIMULADO PSU DE CIENCIAS: %d\n", punt_cs);
		printf("\t\tDIFERENCIA ULTIMO SELEC.: %d\n", (int)(punt_cs - carr[i].pond_min));
		printf("\t\t============================================\n");
		if(punt_cs < carr[i].pond){
			no_postula_cs = 1;
		}
	}
	printf("\n");
	if(psu_lm < carr[i].psu){
		printf("\t\t*Su promedio en psu de Lenguaje y Matematicas es %d,\n\t\tpor lo que no alcanza el minimo de %d para postular*\n\n", psu_lm, carr[i].psu);
	}
	if(no_postula_hist == 1){
		printf("\t\t*Su Ponderacion total con PSU de historia no alcanza\n\t\t\tel minimo de %d para postular*\n\n", carr[i].pond);
	}
	if(no_postula_cs == 1){
		printf("\t\t*Su Ponderacion total con PSU de ciencias no alcanza\n\t\t\tel minimo de %d para postular*\n\n", carr[i].pond);
	}
}

//funcion que realiza el calculo del primedio de los puntajes
void calcularPonderacion(int nem, int rank, int leng, int mate, int hist, int cs, int i, carrera carr[]){
	float div = 100.0;
	int puntaje, psu_lm;
	puntaje = nem*((float)carr[i].nem/div) + rank*((float)carr[i].rank/div) + leng*((float)carr[i].leng/div) + mate*((float)carr[i].mate/div);
	psu_lm = ((leng + mate) / 2);
	mostrarInfoPonderacion(i, carr);	//al tener el puntaje calculado se llama a las funciones mostrarinfo de ponderacion y de puntajes para hacer el despliegue por pantalla de la informacion obtenida
	mostrarInfoPuntajes(div, psu_lm, puntaje, nem, rank, leng, mate, hist, cs, i, carr);
}

void simularPostulacion(carrera carr[]){	//funcion principal de para la simulacion de postulacion en la que se requiere el ingreso de los puntajes al usuario
	int nem, rank, leng, mate, hist, cs, encontrado = 0;
	printf("\n\tIngrese sus puntajes:\n");
	printf("\n\t-NEM:"); scanf("%d", &nem);
	printf("\t-RANK:"); scanf("%d", &rank);
	printf("\t-PSU LENGUAJE:"); scanf("%d", &leng);
	printf("\t-PSU MATEMATICAS:"); scanf("%d", &mate);
	printf("\t-PSU HISTORIA:"); scanf("%d", &hist);
	printf("\t-PSU CIENCIAS:"); scanf("%d", &cs);
	
	do{
		int cod, i = 0;
		printf("\n\tIntroduzca codigo de carrera: ");	//luego de ingresar los datos se le pide al usuario que ingrese el codigo de la carrera a la cual se quiere postular
		scanf("%d", &cod);
		i = buscarCarrera(i, cod, carr);
		if(i != cant_carr){
			calcularPonderacion(nem, rank, leng, mate, hist, cs, i, carr);	//al ser encontrada, se llama a la funcion calcularponderacion
			encontrado = 1;
		}else{
			printf("Codigo de carrera no encontrado, por favor reingrese\n");	//se notifica mensaje de error
		}
	}while(encontrado == 0);

}

void menu(carrera carr[], facultad fac[]){	//funcion que despliega el menu del programa
	int opcion;
    do{
		printf("\n\n\t1. Consultar Ponderación");
		printf("\n\t2. Simular Postulación");
		printf("\n\t3. Mostrar Ponderacions Facultad");
		printf("\n\t4. Salir");
		printf("\n\n\tIntroduzca opción (1-4): ");
		scanf("%d", &opcion);
        switch(opcion){
        	case 1:	//en donde para cada opcion se llama a su respectiva funcion
        		consultarPonderacion(carr);
        		break;
        	case 2:
        		simularPostulacion(carr);
        		break;
        	case 3:
        		mostrarPonderaciones(fac, carr);
        		break;
        }
    }while(opcion != 4);
}

void main(){	//funcion main del programa
	carrera carr[cant_carr];	//se inicializa el arreglo de tipo de dato carrera y facultad con las resspectivas cantidades de estas *constantes al inicio del programa
	facultad fac[cant_fac];
	cargarCarreras("Carreras.txt", carr);	//se llama a las funciones cargarCarreras y facultades guardadas en cargarArchivo.c con su respectivo tipo de dato en carreras.h
	cargarFacultades("Facultades.txt", fac);	//en donde se cargan los archivos carreras.txt y facultades.txt
	menu(carr, fac);	//se carga el menu principal para el arranque del programa por pantalla hacia el usuario
}