#include "carreras.h"
#include <stdio.h>
#include <stdlib.h>

void cargarCarreras(char dir[], carrera carr[]){	//Funcion que carga los datos de las carreras y las almacena en un array de tipo de dato carrera creado en carreras.h
	FILE *arch_carr;
	int i = 0;
	if((arch_carr = fopen(dir,"r")) == NULL){
		printf("\n\n***Error al abrir el archivo***\n\n");
		exit(0);
	}
	else{
		while(feof(arch_carr) == 0){	//Se recorre todo el archivo y se separan los datos por cada ; encontrado en el archivo para almacenarlo a cada atributo del tipo de dato carrera
			fscanf(arch_carr,"%d;%d;%100[^;];%d;%d;%d;%d;%d;%d;%d;%d;%f;%f;%d;%d", 
				&carr[i].cod_fac, &carr[i].cod_carr, carr[i].nom_carr, &carr[i].nem, &carr[i].rank, &carr[i].leng, &carr[i].mate, &carr[i].hist, &carr[i].cs, 
					&carr[i].pond, &carr[i].psu, &carr[i].pond_max, &carr[i].pond_min, &carr[i].cupo_psu, &carr[i].cupo_bea);
			i++;
		}

		fclose(arch_carr);
	}
}

void cargarFacultades(char dir[], facultad fac[]){	//misma funcion para cargar los datos de las facultades
	FILE *arch_fac;
	int i = 0;
	if((arch_fac = fopen(dir,"r")) == NULL){
		printf("\n\n***Error al abrir el archivo***\n\n");
		exit(0);
	}
	else{
		while(feof(arch_fac) == 0){
			fscanf(arch_fac,"%d;%60[^;];", &fac[i].cod_fac, fac[i].nom_fac);
			i++;
		}

		fclose(arch_fac);
	}	
}