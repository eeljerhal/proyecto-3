#ifndef CARRERAS_H
#define CARRERAS_H


typedef struct{
	int cod_fac, cod_carr;
	int nem, rank, leng, mate, hist, cs, pond, psu;
	float pond_max, pond_min;
	int cupo_psu, cupo_bea;
	char nom_carr[100];
} carrera;


typedef struct{
	int cod_fac;
	char nom_fac[60];
} facultad;

void cargarCarreras(char dir[], carrera carr[]);
void cargarFacultades(char dir[], facultad fac[]);

#endif